;; This is an operating system configuration template
;; for a "desktop" setup with GNOME and Xfce.

(use-modules (gnu) (gnu system nss))
(use-service-modules dbus)
(use-service-modules desktop)
(use-service-modules networking)
(use-service-modules ssh)
(use-service-modules vpn)
(use-service-modules web)
(use-service-modules xorg)

(use-package-modules certs)
(use-package-modules cups)
(use-package-modules gnome)
(use-package-modules gnuzilla)
(use-package-modules gtk)
(use-package-modules ibus)
(use-package-modules image-viewers)
(use-package-modules linux)
(use-package-modules mail)
(use-package-modules messaging)
(use-package-modules openbox)
(use-package-modules ssh)
(use-package-modules tls)
(use-package-modules tmux)
(use-package-modules vim)
(use-package-modules web)
(use-package-modules xdisorg)
(use-package-modules xfce)

(define adb-udev-rules
  (udev-rule "99-adb.rules"
  "SUBSYSTEM==\"usb\" ATTRS{idVendor}==\"04e8\", GROUP=\"plugdev\""))

(define adb-udev-service-type
 (service-type (name 'adb-udev)
               (extensions
                (list (service-extension udev-service-type
                        (const (list adb-udev-rules)))
                      (service-extension account-service-type
                        (const (list (user-group (system? #t) (name "plugdev")))))))))

(operating-system
  (host-name "mondas")
  (timezone "Europe/Paris")
  (locale "fr_FR.UTF-8")

  ;; Assuming /dev/sdX is the target hard disk, and "my-root"
  ;; is the label of the target root file system.
  (bootloader (grub-configuration (device "/dev/sdc")))
  (file-systems (cons* (file-system
                        (device "guix")
                        (title 'label)
                        (mount-point "/")
                        (type "ext4"))
                       (file-system
                        (device "boot")
                        (title 'label)
                        (mount-point "/boot")
                        (type "ext2"))
                      %base-file-systems))

  (users (cons (user-account
                (name "tyreunom")
                (comment "tyreunom")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video" "plugdev"))
                (home-directory "/home/tyreunom"))
               %base-user-accounts))

  ;; This is where we specify system-wide packages.
  (packages (cons* nss-certs vim openbox tmux openssh gnutls ;for HTTPS access
                   icecat hexchat claws-mail ibus xfce4-terminal
                   ibus-anthy feh
                   %base-packages))

  ;; Add GNOME and/or Xfce---we can choose at the log-in
  ;; screen with F1.  Use the "desktop" services, which
  ;; include the X11 log-in service, networking with Wicd,
  ;; and more.
  (services (cons* (console-keymap-service "fr-bepo")
                   (service openssh-service-type (openssh-configuration
                                                  (permit-root-login #f)
                                                  (allow-empty-passwords? #f)
                                                  (x11-forwarding? #f)))
                   (static-networking-service "enp4s1" "192.168.1.66"
                                              #:gateway "192.168.1.254"
                                              #:name-servers
                                              (list "80.67.169.12" "10.152.1.1" "10.11.152.66"))
                   (slim-service #:default-user "tyreunom")

                   (service adb-udev-service-type #t)
                   (screen-locker-service xlockmore "xlock")
                   (ntp-service)
                   (elogind-service)
                   (polkit-service)
                   (udisks-service)
                   (dbus-service)
                   (modify-services %base-services
                                    (guix-service-type config =>
                                       (guix-configuration
                                        (inherit config)
                                        (use-substitutes? #t)
                                        (extra-options '("--gc-keep-derivations" "--gc-keep-outputs")))))))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
