;; This is an operating system configuration template
;; for a "bare bones" setup, with no X11 write server.
(use-modules (gnu) (gnu system nss) (gnu services))
(use-service-modules networking ssh web mcron shepherd vpn)
(use-package-modules admin certs gtk messaging php ssh tls tmux vim vpn web)

(define %znc-accounts
  (list (user-group (name "znc") (system? #t))
        (user-account
         (name "znc")
         (group "znc")
         (system? #t)
         (comment "znc server user")
         (home-directory "/srv/znc")
         (shell (file-append shadow "/sbin-nologin")))))

(define (znc-activation conf)
  #~(begin
     (use-modules (guix build utils))
     (mkdir-p "/srv/znc/configs")
     (chown "/srv/znc" (passwd:uid (getpwnam "znc")) (passwd:gid (getpwnam "znc")))
     (chown "/srv/znc/configs" (passwd:uid (getpwnam "znc")) (passwd:gid (getpwnam"znc")))))

(define (znc-shepherd-service conf)
  (let* ((znc-binary (file-append znc "/bin/znc")))
    (list (shepherd-service
           (provision '(znc))
           (documentation "Run the znc daemon.")
           (requirement '(user-processes loopback))
           (start #~(make-forkexec-constructor (list #$znc-binary "-f" "--datadir"
                                                     "/srv/znc")
                       #:user "znc" #:group "znc"
                       #:log-file "/var/log/znc.log"))
           (stop #~(make-kill-destructor))))))
   

(define znc-service-type
  (service-type (name 'znc)
                (extensions
                  (list (service-extension shepherd-root-service-type
                                           znc-shepherd-service)
                        (service-extension activation-service-type
                                           znc-activation)
                        (service-extension account-service-type
                                           (const %znc-accounts))))))

(operating-system
  (host-name "traken")
  (timezone "Europe/Paris")
  (locale "en_US.UTF-8")

  ;; Assuming /dev/sdX is the target hard disk, and "my-root" is
  ;; the label of the target root file system.
  (bootloader (grub-configuration (device "/dev/sda")))
  (file-systems (cons (file-system
                        (device "system")
                        (title 'label)
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))

  ;; don't ever connect to facebook
  ;(host-file (plain-file "hosts"
  ;                       (string-append (local-host-aliases host-name)
  ;                                      %facebook-host-aliases)))

  ;; This is where user accounts are specified.  The "root"
  ;; account is implicit, and is initially created with the
  ;; empty password.
  (users (cons (user-account
                (name "tyreunom")
                (comment "tyreunom")
                (group "users")

                ;; Adding the account to the "wheel" group
                ;; makes it a sudoer.  Adding it to "audio"
                ;; and "video" allows the user to play sound
                ;; and access the webcam.
                (supplementary-groups '("audio" "video"))
                (home-directory "/home/tyreunom"))
               %base-user-accounts))

  ;; Initrd definition
  (initrd (lambda (file-systems . rest)
           ;; Create a standard initrd that has modules "foo.ko"
           ;; and "bar.ko", as well as their dependencies, in
           ;; addition to the modules available by default.
           (apply base-initrd file-systems
                              #:extra-modules '("pata_jmicron" "ahci")
                             rest)))

  ;; Globally-installed packages.
  (packages (cons* openssh tmux vim certbot php znc nss-certs %base-packages))

  ;; Add services to the baseline: a DHCP client and
  ;; an SSH server.
  (services
    (cons*
      (service znc-service-type #t)
      (nginx-service #:config-file "/etc/nginx.conf")
       ; #:vhost-list
       ; (list (nginx-vhost-configuration
       ;          (server-name (list 'default))
       ;          (root "/srv/http/lepiller/site")
       ;          (ssl-certificate "/etc/letsencrypt/live/lepiller.eu/fullchain.pem")
       ;          (ssl-certificate-key "/etc/letsencrypt/live/lepiller.eu/privkey.pem"))))
      (dhcp-client-service)
      (service openssh-service-type (openssh-configuration
                                      (password-authentication? #f)))
      (service openvpn-server-service-type (openvpn-server-configuration
                                             (tls-auth "/etc/openvpn/ta.key")
                                             (cert "/etc/openvpn/traken.crt")
                                             (key "/etc/openvpn/traken.key")
                                             (ca "/etc/openvpn/ca.crt")
                                             (dh "/etc/openvpn/dh4096.pem")))
      (console-keymap-service "fr-bepo")
      (modify-services %base-services
        (guix-service-type config =>
                           (guix-configuration
                             (inherit config)
                             (use-substitutes? #f)
                             (extra-options '("--gc-keep-derivations"))))))))
